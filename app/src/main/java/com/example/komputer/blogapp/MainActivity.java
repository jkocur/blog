package com.example.komputer.blogapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private DatabaseReference mDatabase, likesRef;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    Boolean LikeChecker = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        likesRef = FirebaseDatabase.getInstance().getReference().child("Likes");
        //initialize recyclerview and FIrebase objects
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Blogzone");
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (mAuth.getCurrentUser() == null) {
                    Intent loginIntent = new Intent(MainActivity.this, RegisterActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(loginIntent);

                }
            }
        };}


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        FirebaseRecyclerAdapter<Blogapp, BlogappViewHolder> FBRA = new FirebaseRecyclerAdapter<Blogapp, BlogappViewHolder>(
                Blogapp.class,
                R.layout.card_items,
                BlogappViewHolder.class,
                mDatabase
        )

            {
                @Override
                protected void populateViewHolder(BlogappViewHolder viewHolder, Blogapp model, int position) {
                final String post_key = getRef(position).getKey().toString();
                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDesc());
                viewHolder.setImageUrl(getApplicationContext(), model.getImageUrl());
                viewHolder.setUserName(model.getUsername());

                viewHolder.setLikeButtonStatus(post_key);

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent singleActivity = new Intent(MainActivity.this, SinglePostActivity.class);
                        singleActivity.putExtra("PostID", post_key);
                        startActivity(singleActivity);
                    } });


                    viewHolder.likePostButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            LikeChecker = true;


                            likesRef.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (LikeChecker) {
                                        if (dataSnapshot.child(post_key).hasChild(mAuth.getCurrentUser().getUid())) {
                                            likesRef.child(post_key).child(mAuth.getCurrentUser().getUid()).removeValue();
                                            LikeChecker = false;

                                        } else {
                                            likesRef.child(post_key).child(mAuth.getCurrentUser().getUid()).setValue("Random Value");
                                            LikeChecker = false;

                                        }
                                    }


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }


                    });

                }};
        recyclerView.setAdapter(FBRA);

        }

    public static class BlogappViewHolder extends RecyclerView.ViewHolder{
        View mView;
        ImageButton commentbtn;
        ImageButton likePostButton, commentButton;
        TextView likesNumber;
        int countLikes;
        //  String currentUserId;
        DatabaseReference LikesRef;
        FirebaseAuth mAuth;

        public BlogappViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            commentbtn = (ImageButton) mView.findViewById(R.id.commentBtn);


            LikesRef = FirebaseDatabase.getInstance().getReference().child("Likes");
            mAuth = FirebaseAuth.getInstance();

            LikesRef.keepSynced(true);
            //  currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();

            likePostButton = (ImageButton) mView.findViewById(R.id.dislike_like_button);
            // commentButton = (ImageButton) mView.findViewById(R.id.comment_button);
            likesNumber = (TextView) mView.findViewById(R.id.number_likes);

        }




        public void setLikeButtonStatus(final String Postkey)
        {
            LikesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child(Postkey).hasChild(mAuth.getCurrentUser().getUid()))
                    {
                        countLikes = (int) dataSnapshot.child(Postkey).getChildrenCount();
                        likePostButton.setImageResource(R.drawable.like);
                        likesNumber.setText(Integer.toString(countLikes));
                    }
                    else
                    {
                        countLikes = (int) dataSnapshot.child(Postkey).getChildrenCount();
                        likePostButton.setImageResource(R.drawable.dislike);
                        likesNumber.setText(Integer.toString(countLikes));

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


                public void setTitle(String title){
            TextView post_title = mView.findViewById(R.id.post_title_txtview);
            post_title.setText(title);
        }        public void setDesc(String desc){
            TextView post_desc = mView.findViewById(R.id.post_desc_txtview);
            post_desc.setText(desc);
        }        public void setUserName(String userName){
            TextView postUserName = mView.findViewById(R.id.post_user);
            postUserName.setText(userName);
        }        public void setImageUrl(Context ctx, String imageUrl){
            ImageView post_image = mView.findViewById(R.id.post_image);
            Picasso.with(ctx).load(imageUrl).into(post_image);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_video) {
            Intent set = new Intent(MainActivity.this, VideoActivity.class);
            startActivity(set);
        }       else if (id == R.id.action_add) {
            startActivity(new Intent(MainActivity.this, PostActivity.class));
        } else if (id == R.id.logout){
            mAuth.signOut();
            Intent logouIntent = new Intent(MainActivity.this, RegisterActivity.class);
            logouIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logouIntent);
        }        return super.onOptionsItemSelected(item);
    }
}
